Release notes
=============
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

[Unreleased]
------------

- no unreleased changes

[4.0.x]
-------

### [4.0.0] - 2022-06-29

#### Changed
- Joomla 4 compatibility
- Minimal Joomla 3.10 requirement
- Minimal PHP 7.2 requirement
- Improved install script

[3.0.x]
-------

### [3.0.1] - 2017-05-17

#### Changed
- New Update server

### [3.0.0] - 2016-01-17

#### Changed
- Joomla 2.5 support discontinued, Joomla 3.4 only and later is supported now
- Translations are not included in basic installation anymore
- New Help site
- New Update server 

[2.1.x]
------]

### [2.1.0] - 2015-07-03

#### Added
- new additional syntax: {lang !en}For all languages except english{/lang}

[2.0.x]
-------

### [2.0.2] - 2012-12-06

#### Changed
- Joomla 3.0 compatibility
- Joomla! 1.6 and 1.7 are not further supported
- PHP 5 Strict Standards improvements
#### Added
- added it-IT translation - thanks to Fabio Perri at Transifex

### [2.0.1] - 2012-08-18

#### Added
- External help added

### [2.0.0] - 2012-05-24

#### Added
- Update server added

[1.6.x]
-------

### [1.6.0] - 2011-06-20

- Initial release