Credits
=======

Thanks to these projects and contributors:

- [Joomla! CMS][Joomla]
- [Bongovo!][BONGOVO] for Czech and Slovak translations
- [Czech Joomla community][JOOMLAPORTAL] for testing, support and comments on development

[JOOMLA]: https://www.joomla.org
[BONGOVO]: https://www.bongovo.cz/
[JOOMLAPORTAL]: https://www.joomlaportal.cz/
