Usage
=====

Usage of n3tLangFilter is very simple. Basic syntax looks like this:

    All languages text
    {lang en}English text{/lang}
    {lang de}German text{/lang}
    {lang !en}Any language but english{/lang}

In the example above: 

 * the first line will be shown to all users
 * the second line only to users with currently selected english
 * the third line only to users with german langugae selected
 * the last line to all users with other then English language selected

Detailed explanation
--------------------

The general syntax looks like this:

    {lang code}Content{/lang}

where:

 * `code` - This is the Url language code of the language, which you would like to 
   display. You can find this code in the Language manager in your administration.
 * `Content` - This is the content for the selected language. It could contain any 
   HTML tags, or even any other content plugins code. If you use other plugins within 
   the n3tLangFilter tags, make sure that n3tLangFilter is published before these plugins, 
   to make loading of the page faster.
  
Advanced syntax could also look like this:  

    {lang !code}Content{/lang}

`code` and `Content` meaning is same as above, the exclamation mark (`!`) means negation, 
it means the `Content` will be displayed in all languages except the one identified by `code`.   