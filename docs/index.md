n3t Language Filter
===================

n3t Language Filter is simple Joomla! plugin enabling to write multilingual contents in one article.


Installation
------------

n3t Language Filter is Joomla! content plugin. It could be installed as any other extension in 
Joomla!

After installing __do not forget to enable the plugin__ from Plugin Manager in your
Joomla! installation.