n3t Language Filter
===================

__Enable multilanguage for your Joomla! installation.__

n3t Language Filter will not work, unless you enable the multilanguage options 
for your site.
 
To do this just follow these simple steps:

 * Go to the Plugin Manager and enable plugin called "System - Language Filter". 
   Check also its options, you can choose some options in its settings. 
 * Go to Module Manager and insert new module type "Language Switcher" somewhere 
   to your template. Read general Joomla! help to get more information about publishing 
  and positioning modules. Again, you will find some options in this module, check it out. 
 * Install additional language packages to your Joomla!, if you didn't this yet. 
   You can install languages pack through your Extensions manager. Since Joomla! 2.5.7 
   there is available option "Install Languages", where you can easily choose from 
   available languges packs distributed by official translations teams all around the world. 
 * Go to the Language Manager and select your default language for admininstartion and the site. 
 * Go to the Language Manager, page "Content" and insert content language for each 
   of your installed language pack. Note, that you have to fill the title, native title, 
   url code, language tag and few more details for all of your installed languages. 
   More detail you can find in Joomla! general help. 
   Notice, that the url code is not used only for the url, but also n3tLangFilter 
   uses this to detect correct language. Finally make sure, that all of your 
   selected languages are published, and with correct access level. 
 * Finally, go to the plugin manager and make sure that n3t Language Filter plugin 
   is published with correct access level. In most cases this should be "Public". 