n3t Language Filter
===================

[![Joomla! CMS](https://img.shields.io/badge/Joomla!%20CMS-v3.10-green)][JOOMLA]
[![Joomla! CMS](https://img.shields.io/badge/Joomla!%20CMS-v4.x-green)][JOOMLA]
[![PHP](https://img.shields.io/badge/PHP-v7.2-green)][PHP]
[![Documentation Status](https://readthedocs.org/projects/n3t-language-filter/badge/?version=latest)][DOCS]

n3t Language Filter is simple Joomla! plugin enabling to write multilingual contents in one article.

Installation
------------

n3t Language Filter is Joomla! content plugin. It could be installed as any other extension in 
Joomla!

After installing **do not forget to enable the plugin** from Plugin Manager in your
Joomla! installation.

Documentation
-------------

Detailed documentation could be found at [n3t Langauge Filter documentation page][DOCS].

[JOOMLA]: https://www.joomla.org
[PHP]: https://www.php.net
[DOCS]: http://n3tlanguagefilter.docs.n3t.cz/