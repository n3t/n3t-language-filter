<?php
/**
 * @package n3t Language Filter
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2010 - 2022 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\CMS\Factory;
use Joomla\CMS\Language\LanguageHelper;
use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\CMS\Version;

class PlgContentN3tLangFilter extends CMSPlugin {

	public function onContentPrepare($context, &$article, &$params, $page = 0)
	{		
		if (strpos( $article->text, '{lang' ) === false)
			return true;

		if (Version::MAJOR_VERSION >= 4)
			$lang = Factory::getApplication()->getLanguage();
		else
			$lang = Factory::getLanguage();

    $lang_codes 	= LanguageHelper::getLanguages('lang_code');

    if (isset($lang_codes[$lang->getTag()])) {
      $current_lang = $lang_codes[$lang->getTag()];

			$regex = '~{lang ' . $current_lang->sef . '}(.*?){/lang}~is';
		  $article->text = preg_replace($regex,'$1', $article->text);

		  $regex = '~{lang !' . $current_lang->sef . '}(.*?){/lang}~is';
		  $article->text = preg_replace($regex,'', $article->text);
    }
      
		$regex = '~{lang ![^}]+}(.*?){/lang}~is';
		$article->text = preg_replace($regex,'$1', $article->text);

		$regex = '~{lang [^}]+}.*?{/lang}~is';
		$article->text = preg_replace($regex,'', $article->text);

  	return true;
	}
	
}